export const handleInputSignup = 'HANDLE_INPUT_SIGNUP';

export const requestSignup = 'REQUEST_API_SIGNUP';

export const signupSuccess = 'SIGNUP_SUCCESS';

export const responseWithError = 'RESPONSE_WITH_ERROR_SIGNUP';

export const closePopup = 'CLOSE_POPUP_SIGNUP';
