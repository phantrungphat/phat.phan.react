export const handleInputEdit = 'HANDLE_INPUT_EDITPROFILE';

export const requestEditProfile = 'REQUEST_EDIT_PROFILE';

export const editProfileSuccess = 'EDIT_PROFILE_SUCCESS';

export const getProfileNow = 'GET_PROFILE_NOW';