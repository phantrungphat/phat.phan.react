export const handleInput = 'HANDLE_INPUT_FORGOT_PASSWORD';

export const requestChangeForgot = 'REQUEST_CHANGE_FORGOT_PASSWORD';

export const forgotPasswordSuccess = 'FORGOT_PASSWORD_SUCCESS';
