export const requestGetProfile = 'REQUEST_GET_PROFILE';

export const getProfileSuccess = 'GET_PROFILE_SUCCESS';

export const toEditProfile = 'REDIRECT_TO_EDITPROFILE';

export const setDefaultRedirect = 'SET_DEFAULT_REDIRECT';