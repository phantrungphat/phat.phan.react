export const handleInput = 'HANDLE_INPUT_CHANGE_PASSWORD';

export const requestChangePassword = 'REQUEST_CHANGE_PASSWORD';

export const changePasswordSuccess = 'CHANGE_PASSWORD_SUCCESS';

export const inChangePassword = 'IN_CHANGE_PASSWORD';

export const leaveChangePassword = 'LEAVE_CHANGE_PASSWORD';