export const apiSignin = 'signin';

export const apiProfile = 'user/view';

export const apiNewsFeed = 'user/newfeed/view';

export const apiPostNewsFeed = 'user/newfeed/post';

export const apiSignup = 'signup';

export const apiEditProfile = 'user/edit';

export const apiGetListFriend = 'user/friend/viewlist';

export const apiChangePassword = 'user/changepassword';

export const apiForgotPassword = 'forgotpassword';

export const apiRequestForgot = 'requesforgotpassword';