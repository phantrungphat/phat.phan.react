import { takeLatest } from 'redux-saga/effects';
import { requestSignin } from './../constants/actions/signin';
import { requestSignup } from './../constants/actions/signup';
import { requestGetProfile } from './../constants/actions/profile';
import { requestEditProfile } from './../constants/actions/editProfile';
import { requestPostNewsFeed } from './../constants/actions/formPost';
import { requestGetNewsFeed } from './../constants/actions/newsfeed';
import { getFriendList } from './../constants/actions/friendList';
import { requestChangePassword } from './../constants/actions/changePassword';
import { requestForgotPassword } from './../constants/actions/requestForgot';
import { requestChangeForgot } from './../constants/actions/forgotPassword';
import signinPage from './../containers/signinPage/saga';
import signupPage from './../containers/signupPage/saga';
import profilePage from './../containers/profilePage/saga';
import newsfeedPage from './../containers/newsfeedPage/saga';
import formPost from './../containers/formPost/saga';
import editProfilePage from './../containers/editProfile/saga';
import friendList from './../containers/friendList/saga';
import changePassword from './../containers/changePassword/saga';
import requestForgot from './../containers/requestForgot/saga';
import forgotPassword from './../containers/forgotPassword/saga';

function* rootSaga() {
    yield takeLatest(requestSignin, signinPage);
    yield takeLatest(requestSignup, signupPage);
    yield takeLatest(requestGetProfile, profilePage);
    yield takeLatest(requestEditProfile, editProfilePage);
    yield takeLatest(requestPostNewsFeed, formPost);
    yield takeLatest(requestGetNewsFeed, newsfeedPage);
    yield takeLatest(getFriendList, friendList);
    yield takeLatest(requestChangePassword, changePassword);
    yield takeLatest(requestForgotPassword, requestForgot);
    yield takeLatest(requestChangeForgot, forgotPassword);
}

export default rootSaga;