import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import 'font-awesome/css/font-awesome.min.css';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import configureStore from './redux/configureStore';
import { createBrowserHistory } from 'history';

export const history = createBrowserHistory();
const store = configureStore();

ReactDOM.render(<BrowserRouter>
    <Provider store={ store }> 
        <ConnectedRouter history={history}>
            <App />
        </ConnectedRouter>
    </Provider>
</BrowserRouter>, document.getElementById('content'));

serviceWorker.unregister();