import {
    createStore,
    compose,
    applyMiddleware
} from 'redux';
import rootReducer from './../reducers';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './../sagas';
import { routerMiddleware } from 'connected-react-router';
import {history} from './../';

const composeEnhancers = process.env.NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        shouldHotReload: false,
    })
    :compose;
    
const sagaMiddleware = createSagaMiddleware();

const configureStore = () => {
    const middlewares = [sagaMiddleware];
    const enhancers = [applyMiddleware(...middlewares, routerMiddleware(history))];
    const store = createStore(rootReducer(history), composeEnhancers(...enhancers));
    sagaMiddleware.run(rootSaga);
    return store;
}

export default configureStore;