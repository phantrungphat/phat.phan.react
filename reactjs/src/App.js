import React from 'react';
import Signin from './containers/signinPage';
import Signup from './containers/signupPage';
import Profile from './containers/profilePage';
import EditProfile from './containers/editProfile';
import NavBar from './containers/navbar';
import { Route, Router } from "react-router-dom";
import NewsFeed from './containers/newsfeedPage';
import { history } from './';
import PrivateRoute from './components/privateRoute';
import FriendList from './containers/friendList';
import ChangePassword from './containers/changePassword';
import ForgotPassword from './containers/forgotPassword';
import RequestForgot from './containers/requestForgot';


import './App.css';

class App extends React.Component {
    render() {
        return (
            <Router history={history}>
                <div className="App">
                    <Route path="/" component={NavBar} />
                    <Route exact path="/signin" component={Signin} />
                    <PrivateRoute exact path="/profile" component={Profile} />
                    <PrivateRoute exact path="/" component={NewsFeed} />
                    <Route exact path="/signup" component={Signup} />
                    <PrivateRoute exact path="/editprofile" component={EditProfile} />
                    <PrivateRoute exact path="/friendlist" component={FriendList} />
                    <PrivateRoute exact path="/changepassword" component={ChangePassword} />
                    <Route path="/forgotpassword/:token" component={ForgotPassword} />
                    <Route path="/requestforgot" component={RequestForgot} />
                </div>
            </Router>
        );
    }
}
export default App;
