import React from 'react';

class Link extends React.Component {
    render() {
        const {classSymmetric, content, href} = this.props;
        return (
            <div className="d-flex justify-content-center links">
                {this.props.children} <a href={href} className={classSymmetric}>{content}</a>
            </div>
        );
    }
}

export default Link;
