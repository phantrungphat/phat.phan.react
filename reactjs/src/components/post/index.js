import React from 'react';
import './style.css';
export const Post = (props) => {
  return (
    <div>
      <div className="post-slide5">
        <div className="post-review">
          <p className="post-description">
            {props.content}
          </p>
          <div className="post-bar">
            <span><i className="fa fa-user" /> <a href="/">{props.emailOrPhone}</a></span>
            <span className="comments"><i className="fa fa-comments" /> <a href="/">{props.postAt}</a></span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Post;
