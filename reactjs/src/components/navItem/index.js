import React from 'react';
import './style.css';

export const NavItem = (props) => {
    return (
        <div>
            <li className="nav-item">
                <a className="nav-link" href='/'>
                    <i className= { props.classIcon}>
                        <span className={props.classNote}>{props.numberNode}</span>
                    </i>
                    {props.itemName}
                </a>
            </li>
        </div>
    );
}