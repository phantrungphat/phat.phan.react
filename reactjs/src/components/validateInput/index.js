import React from 'react';
import './style.css';

export const Validation = (props) => {
  return (
    <div>
      <div className="textError text-center"> { props.message }</div>
    </div>
  );
}
