import {history} from './../../';
export const checkAuth = () => {
    if (localStorage.getItem('token')) history.push({ pathname: '/' });
}