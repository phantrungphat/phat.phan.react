import React from 'react';
import './../../../node_modules/font-awesome/css/font-awesome.min.css'; 

export const Input = (props) => {
  return (
    <div>
      <div className="input-group mb-2">
          <div className="input-group-append">
              <span className="input-group-text"><i className={props.classIcon} /></span>
          </div>
          <input 
              type= { props.type } 
              className="form-control input_user" 
              placeholder= { props.placeholder }
              onChange = {event => props.onInputChange(event) }
          />
      </div>
    </div>
  );
}
