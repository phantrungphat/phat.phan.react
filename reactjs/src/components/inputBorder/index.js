import React from 'react';

import './style.css';

export const InputBorder = (props) => {
    return (
        <div className="form-label-group">
            <input defaultValue={props.defaultValue} onChange = {props.onInputChange} type={props.type} id={props.idLabel} className="form-control" placeholder={props.plholder} required />
            <label htmlFor={props.idLabel}>{props.plholder}</label>
        </div>
    );
}
