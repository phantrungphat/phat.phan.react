import React from 'react';
import { InputBorder } from './../../components/inputBorder';
import { Validation } from './../../components/validateInput';
import { connect } from 'react-redux';
import * as actions from './actions';
import { checkAuth } from './../../components/checkAuth';
import GlobalLoading from './../globalLoading';
import Loading from './../globalLoading';
import Popup from './../../containers/popup';

import './style.css';

class signupPage extends React.Component {

    componentWillMount() {
        checkAuth();
    }

    handleInput = (key, event) => {
        this.props.onHandleInput(key, event.target.value);
    }

    handleSubmit = (evt) => {
        evt.preventDefault();
        if (this.props.handleInput.isValidEmail && this.props.handleInput.isValidPassword && this.props.handleInput.isValidRePassword) {
            this.props.requestSignup();
        }
    }

    render() {
        const { emailError, passwordError, rePasswordError, aboutError } = this.props.handleInput;
        return (
            <div>
                <Loading />
                <Popup />
                <div className="container">
                    <div className="row">
                        <div className="col-lg-10 col-xl-9 mx-auto">
                            <div className="card card-signin flex-row my-5">
                                <div className="card-img-left d-none d-md-flex">
                                </div>
                                <div className="card-body">
                                    <h5 className="card-title text-center"><strong>Sign Up</strong></h5>
                                    <GlobalLoading />
                                    <form onSubmit={this.handleSubmit} className="form-signin">
                                        <InputBorder onInputChange={value => this.handleInput('emailOrPhone', value)} type='email' idLabel='inputEmail' plholder='Email' />
                                        <Validation message={emailError}></Validation>
                                        <hr />
                                        <InputBorder onInputChange={value => this.handleInput('password', value)} type='password' idLabel='inputPassword' plholder='Password' />
                                        <Validation message={passwordError}></Validation>
                                        <InputBorder onInputChange={value => this.handleInput('rePassword', value)} type='password' idLabel='inputCofirmPassword' plholder='Confirm Password' />
                                        <Validation message={rePasswordError}></Validation>
                                        <hr />
                                        <InputBorder onInputChange={value => this.handleInput('dob', value)} type='date' idLabel='inputDayOfBirth' plholder='Day of Birth' />
                                        <InputBorder onInputChange={value => this.handleInput('about', value)} type='text' idLabel='inputAbout' plholder='About you' />
                                        <Validation message={aboutError}></Validation>
                                        <div onChange={event => this.handleInput('gender', event)} className="form-label-group">
                                            <input type="radio" name="gender" className="" value="male" required /> Male
                                            <input type="radio" name="gender" className="ml-2" value="female" required /> Female
                                        </div>

                                        <button className="btn btn-lg btn-success btn-block text-uppercase" type="submit">Signup</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        handleInput: state.HandleInputSignup
    }
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        onHandleInput: (key, value) => {
            dispatch(actions.handleInputSignup(key, value));
        },
        requestSignup: () => {
            dispatch(actions.requestSignup());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(signupPage);
