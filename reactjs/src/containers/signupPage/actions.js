import * as types from './../../constants/actions/signup';

export const handleInputSignup = (key, value) => {
    return {
        type: types.handleInputSignup,
        key,
        value
    }
};

export const requestSignup = () => {
    return {
        type: types.requestSignup,
    }
};

export const signupSuccess = () => {
    return {
        type: types.signupSuccess,
    }
};