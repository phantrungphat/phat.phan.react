import * as configHost from './../../constants/host/config';
import * as configApi from './../../constants/api/config';
import { fork, select, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import { showLoading, hideLoading } from './../globalLoading/actions';
import {responseWithError} from './../popup/actions';

function* signupApi() {
    try {
        const getStateSignup = yield select(state => state.HandleInputSignup);
        let params = {
            emailOrPhone: getStateSignup.emailOrPhone,
            password: getStateSignup.password,
            dob: getStateSignup.dob,
            about: getStateSignup.about,
            gender: getStateSignup.gender
        };
        const data = yield fetch(`${configHost.apiUrl}/${configApi.apiSignup}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        });

        const respone = yield data.json();
        yield put(hideLoading());
        if (!respone.error) {
            yield put(push('/signin'));
        }
        else {
            yield put(responseWithError(respone.errors[0].errorMessage));
        }
    }
    catch (error) {
        yield put(responseWithError(error));
    }
}

export default function* signupPage() {
    yield put(showLoading());
    yield fork(signupApi);
}
