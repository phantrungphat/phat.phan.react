import * as types from './../../constants/actions/signup';
import {checkLength} from './../../components/checkLength';

const initialState = {
    emailOrPhone: '',
    password: '',
    dob: '',
    about: '',
    gender: 'male',
    emailError: '',
    passwordError: '',
    rePasswordError: '',
    aboutError: '',
    isValidEmail: false,
    isValidPassword: false,
    isValidRePassword: false,
    isValidAbout: false,
    showLoading: false
};

var isValidEmail = (emailOrPhone) => {
    try {
        if (!/^[a-zA-Z0-9._%+-]{2,}@[a-zA-Z0-9.-]{2,}(\.[a-z]{2,})$/.test(emailOrPhone)) {
            return false;
        } return true;
    } catch (error) {
        alert('Something went wrong!!!');
    }
}

var checkLengthPwd = (password) => {
    try {
        if (password.length < 6) {
            return 'The password too Short';
        }
        else if (password.length > 16) {
            return 'The password too Long';
        }
        else return '';
    } catch (error) {
        alert(error);
    }
}

var isValidRePassword = (password, rePassword) => {
    try {
        if (password === '') return false;
        if (password !== rePassword) {
            return false;
        } return true;
    } catch (error) {
        alert(error);
    }
}

var signupReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.handleInputSignup:
            if (action.key === 'emailOrPhone') {
                if (isValidEmail(action.value)) {
                    state.emailError = '';
                    state.isValidEmail = true;
                }
                else {
                    state.emailError = 'The email is invalid';
                    state.isValidEmail = false;
                } 
            } else if (action.key === 'password') {
                let checkPassword = checkLengthPwd(action.value);
                if (checkPassword !== '') {
                    state.isValidPassword = false;
                }
                else {
                    state.isValidPassword = true;
                }
                state.passwordError = checkPassword;
            }
            else if (action.key === 'rePassword') {
                if (isValidRePassword(state.password, action.value))
                {
                    state.rePasswordError = '';
                    state.isValidRePassword = true;
                }
                else {
                    state.rePasswordError = 'Password and Comfirm Password are not match';
                    state.isValidRePassword = false;
                }
            }
            else if (action.key === 'about') {
                let checkAbout = checkLength(action.value);
                if (checkAbout !== '') {
                    state.isValidAbout = false;
                }
                else {
                    state.isValidAbout = true;
                }
                state.aboutError = checkAbout;
            }
            return {
                ...state,
                [action.key]: action.value
            };
        default:
            return state;
    }
};

export default signupReducer;