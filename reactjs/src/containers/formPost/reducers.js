import * as types from './../../constants/actions/formPost';

const initialState = {
    dataNewPost: '',
    error: false,
    errorMessage: ''
};

var formPost = (state = initialState, action) => {
    switch (action.type) {
        case types.getDataNewPost:
            return {
                ...state,
                dataNewPost: action.content
            };
        case types.postOnNewsFeedSuccess:
            return {
                ...state,
                dataNewPost: ''
            };
        default:
            return state;
    }
};

export default formPost;