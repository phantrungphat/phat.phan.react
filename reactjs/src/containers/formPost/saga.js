import * as configHost from './../../constants/host/config';
import * as configApi from './../../constants/api/config';
import { fork, select, put } from 'redux-saga/effects';
import { postOnNewsFeedSuccess } from './actions';
import { requestGetNewsFeed } from './../newsfeedPage/actions';
import { responseWithError } from './../popup/actions';
import { showLoading, hideLoading } from './../globalLoading/actions';

function* postOnNewsFeedApi() {
    try {
        const getContentPost = yield select(state => state.postNewsFeed);
        const token = yield localStorage.getItem('token');
        const data = yield fetch(`${configHost.apiUrl}/${configApi.apiPostNewsFeed}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'apiToken': token
            },
            body: JSON.stringify({ content: getContentPost.dataNewPost })
        });
        const respone = yield data.json();
        yield put(hideLoading());
        if (!respone.error) {
            yield put(postOnNewsFeedSuccess());
            yield put(requestGetNewsFeed());
        }
        else {
            localStorage.clear();
            console.log(respone.errors);
            yield put(responseWithError(respone.errors));
        }
    }
    catch (error) {
        yield put(hideLoading());
        yield put(responseWithError(error));
    }
}

export default function* formPost() {
    yield put(showLoading());
    yield fork(postOnNewsFeedApi);
}
