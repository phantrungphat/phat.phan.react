import React from 'react';
import './style.css';
import { connect } from 'react-redux';
import * as actions from './actions';
import Loading from './../globalLoading';
import Popup from './../../containers/popup';

class FormPost extends React.Component {

    handlePost = (event) => {
        this.props.getDataNewPost(event.target.value);
    }

    handleSubmit = (evt) => {
        evt.preventDefault();
        if (this.props.postNewsFeed.dataNewPost === '') alert('Nothing to Post!!!');
        else this.props.requestPostNewsFeed();
    }

    render() {
        const { dataNewPost } = this.props.postNewsFeed;
        return (
            <div className="col-md-12">
                <Loading />
                <Popup />
                <form onSubmit={evt => this.handleSubmit(evt)}>
                    <div className="form-post">
                        <div className="form-group green-border-focus">
                            <textarea onChange={evt => this.handlePost(evt)} placeholder="Start posting today..." className="form-control color-form-post" rows="4" value={dataNewPost} />
                        </div>
                        <button type="submit" className="btn btn-outline-success btn-post">Post on your NewsFeed</button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        postNewsFeed: state.postNewsFeed,
    }
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        getDataNewPost: (content) => {
            dispatch(actions.getDataNewPost(content));
        },
        requestPostNewsFeed: (content) => {
            dispatch(actions.requestPostNewsFeed(content));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(FormPost);