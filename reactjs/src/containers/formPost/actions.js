import * as types from './../../constants/actions/formPost';

export const requestPostNewsFeed = (content) => {
    return {
        type: types.requestPostNewsFeed,
        content
    }
};

export const postOnNewsFeedSuccess = () => {
    return {
        type: types.postOnNewsFeedSuccess
    }
};

export const getDataNewPost = (content) => {
    return {
        type: types.getDataNewPost,
        content
    }
};