import * as types from './../../constants/actions/requestForgot';

export const handleInput = (key, value) => {
    return {
        type: types.handleInput,
        key,
        value
    }
};

export const requestForgotPassword = () => {
    return {
        type: types.requestForgotPassword,
    }
};

