import React from 'react';
import { InputBorder } from './../../components/inputBorder';
import * as actions from './actions';
import { connect } from 'react-redux';
import { Validation } from './../../components/validateInput';
import './style.css';
import Loading from './../globalLoading';
import Popup from './../../containers/popup';

class RequestForgot extends React.Component {

    handleInput = (key, event) => {
        this.props.handleInput(key, event.target.value);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if (this.props.requestForgot.isValidEmail) 
            this.props.requestForgotPassword();
    }
    render() {
        const { emailError } = this.props.requestForgot;
        return (
            <div>
                <Loading />
                <Popup />
                <div className="container-fluid">
                    <div className="row d-flex justify-content-center edit-height">
                        <div className="col-md-6">
                            <div className="card card-change-password flex-row">
                                <div className="card-body">
                                    <h5 className="card-title text-center mb-4"><strong>Forgot Password</strong></h5>
                                    <form onSubmit={this.handleSubmit} className="form-signin">
                                        <InputBorder
                                            onInputChange={value => this.handleInput('emailOrPhone', value)}
                                            type='email' idLabel='Email'
                                            plholder='Enter your Email'
                                        />
                                        <Validation message={emailError}></Validation>
                                        <button className="btn btn-lg btn-success btn-block text-uppercase btn-editprofile" type="submit">send</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        requestForgot: state.requestForgot
    }
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        handleInput: (key, value) => {
            dispatch(actions.handleInput(key, value));
        },
        requestForgotPassword: () => {
            dispatch(actions.requestForgotPassword());
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(RequestForgot);