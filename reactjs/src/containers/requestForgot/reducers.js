import * as types from './../../constants/actions/requestForgot';

const initialState = {
    emailOrPhone: '',
    emailError: '',
    isValidEmail: false,
};

var isValidEmail = (emailOrPhone) => {
    try {
        if (!/^[a-zA-Z0-9._%+-]{2,}@[a-zA-Z0-9.-]{2,}(\.[a-z]{2,})$/.test(emailOrPhone)) {
            return false;
        } return true;
    } catch (error) {
        alert('Something went wrong!!!');
    }
}

var requestForgot = (state = initialState, action) => {
    switch (action.type) {
        case types.handleInput:
            if (action.key === 'emailOrPhone') {
                if (isValidEmail(action.value)) {
                    state.emailError = '';
                    state.isValidEmail = true;
                }
                else {
                    state.emailError = 'The email is invalid';
                    state.isValidEmail = false;
                }
            }
            return {
                ...state,
                [action.key]: action.value,
            };
        default:
            return state;
    }
};

export default requestForgot;