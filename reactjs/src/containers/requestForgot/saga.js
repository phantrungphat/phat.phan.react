import * as configHost from './../../constants/host/config';
import * as configApi from './../../constants/api/config';
import { select, fork, put } from 'redux-saga/effects';
import {showLoading, hideLoading} from './../globalLoading/actions';
import {responseWithError} from './../popup/actions';

function* requestForgotApi() {
    try {
        const getStateRequestForgot = yield select(state => state.requestForgot);
        let params = {
            email: getStateRequestForgot.emailOrPhone
        };
        console.log(params);
        const data = yield fetch(`${configHost.apiUrl}/${configApi.apiRequestForgot}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        });
        const respone = yield data.json();
        yield put(hideLoading());
        if (!respone.error) {
            yield put(responseWithError('Email is sent, please check your Email'));
        }
        else {
            yield put(responseWithError(respone.errors.errorMessage));
        }
    }
    catch (error) {
        yield put(hideLoading());
        yield put(responseWithError(error));
    }
}
export default function* requestForgotPassword() {
    yield put(showLoading());
    yield fork(requestForgotApi);
}
