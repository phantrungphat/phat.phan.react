import * as configHost from './../../constants/host/config';
import * as configApi from './../../constants/api/config';
import { put, fork } from 'redux-saga/effects';
import { getNewsFeedSuccess } from './actions';
import { push } from 'connected-react-router';
import {responseWithError} from './../popup/actions';
import {showLoading, hideLoading} from './../globalLoading/actions';


function* getNewsFeedApi() {
    try {
        const token = yield localStorage.getItem('token');
        const data = yield fetch(`${configHost.apiUrl}/${configApi.apiNewsFeed}`, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'apiToken': token
            },
        });
        const respone = yield data.json();
        yield put(hideLoading());
        if (!respone.error) {
            yield put(getNewsFeedSuccess(respone.data));
        }
        else {
            localStorage.clear();
            yield put(push('/signin'));
            yield put(responseWithError(respone.errors));
        }
    }
    catch (error) {
        yield put(hideLoading());
        yield put(responseWithError(error));
    }
}

export default function* newsfeedPage() {
    yield put(showLoading());
    yield fork(getNewsFeedApi);
}
