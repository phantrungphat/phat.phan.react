import * as types from './../../constants/actions/newsfeed';
import { postOnNewsFeedSuccess } from './../../constants/actions/formPost';
import { onSearch } from './../../constants/actions/navbar';

const initialState = {
    posts: [],
    dataNewPost: '',
    emailOrPhone: '',
    myPosts: []
};

var newsfeedReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.getNewsFeedSuccess:
            state.emailOrPhone = localStorage.getItem('emailOrPhone');;
            state.myPosts = action.posts;
            state.myPosts = state.myPosts.filter((myPost) => {
                return myPost.email_phone.toLowerCase().indexOf(state.emailOrPhone) !== -1;
            });
            return {
                ...state,
                posts: action.posts,
            };
        case postOnNewsFeedSuccess:
            return {
                ...state,
                dataNewPost: ''
            };
        case onSearch:
            return {
                ...state,
                posts: action.value
            };
        default:
            return state;
    }
};

export default newsfeedReducer;