import * as types from './../../constants/actions/newsfeed';

export const requestGetNewsFeed = () => {
    return {
        type: types.requestGetNewsFeed,
    }
};

export const getNewsFeedSuccess = (posts) => {
    return {
        type: types.getNewsFeedSuccess,
        posts
    }
};