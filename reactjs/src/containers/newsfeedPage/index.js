import React from 'react';
import { Post } from './../../components/post';
import Profile from './../profilePage';
import FormPost from './../formPost';
import './style.css';
import { connect } from 'react-redux';
import * as actions from './actions';
import EditProfile from './../editProfile';
import FriendList from './../friendList';
import Loading from './../globalLoading';
import Popup from './../../containers/popup';

class NewsFeed extends React.Component {

    componentWillMount() {
        // this.props.getFriendRequest();
        this.props.requestGetNewsFeed();
        if (!localStorage.getItem('token')) this.props.history.push({ pathname: '/signin' });
    }

    render() {
        const { posts, myPosts } = this.props.newsfeed;
        // const {listFriendRequest} = this.props.friendRequest;
        return (
            <div>
                <Loading />
                <Popup />
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-md-3 edit-color-height">
                            {
                                this.props.getMoveProfile.redirect &&
                                <Profile />
                            }
                        </div>
                        {
                            this.props.getMoveProfile.redirect && !this.props.toEditProfile.redirect &&
                            <div className="col-md-6">
                                <div className="owl-carousel">
                                    {
                                        myPosts.map((myPost, index) => {
                                            return <Post key={index} emailOrPhone={myPost.email_phone} content={myPost.content}
                                                postAt={myPost.updated_at} />
                                        })
                                    }
                                </div>
                            </div>
                        }
                        {
                            !this.props.getMoveProfile.redirect &&
                            <div className="col-md-6">
                                <FormPost />
                                <div className="owl-carousel">
                                    {
                                        posts.map((post, index) => {
                                            return <Post key={index} emailOrPhone={post.email_phone} content={post.content}
                                                postAt={post.updated_at} />
                                        })
                                    }
                                </div>
                            </div>
                        }
                        {
                            this.props.getMoveProfile.redirect && this.props.toEditProfile.redirect &&
                            <div className="col-md-6">
                                <EditProfile />
                            </div>
                        }

                        {
                            this.props.getMoveProfile.redirect &&
                            <div className="col-md-3 friend-list">
                                <FriendList />
                            </div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        newsfeed: state.NewsFeed,
        getMoveProfile: state.MoveProfile,
        toEditProfile: state.Profile,
        infoSignin: state.HandleInputSignin
    }
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        requestGetNewsFeed: () => {
            dispatch(actions.requestGetNewsFeed());
        },
        getNewsFeedSuccess: (data) => {
            dispatch(actions.getNewsFeedSuccess(data));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsFeed);