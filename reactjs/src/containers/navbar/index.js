import React from 'react';
import './style.css';
import { NavItem } from './../../components/navItem';
import { connect } from 'react-redux';
import * as actions from './actions';
import { history } from './../../';
import { leaveChangePassword } from './../changePassword/actions';

class NavBar extends React.Component {

    handleMoveProfile = (event) => {
        this.props.leaveChangePassword();
        if (this.props.changePassword.inChangePassword) {
            history.push({ pathname: '/' });
            this.props.setRedirectProfile();
        }
        event.preventDefault();
        this.props.setRedirectProfile();
    }
    check = (evt) => {
        evt.preventDefault();
        localStorage.clear();
        history.push({ pathname: '/signin' });
    }

    changePassword = (evt) => {
        evt.preventDefault();
        history.push({ pathname: '/changepassword' });
    }

    handleSearch = (evt) => {
        this.props.getInputSearch(evt.target.value);
        this.props.onSearch(this.props.getMoveProfile.postsSearch);
    }

    handleOnSearch = (evt) => {
        evt.preventDefault();
        this.props.onSearch(this.props.getMoveProfile.postsSearch);
    }

    render() {
        return (
            <div>
                {
                    localStorage.getItem('token') &&
                    <nav className="navbar navbar-icon-top navbar-expand-lg navbar-dark">
                        <a className="navbar-brand" href='/'>BSS - Intern</a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon" />
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav mr-auto">
                                <li className="nav-item">
                                    <a className="nav-link" href='/' onClick={evt => this.handleMoveProfile(evt)}>
                                        <i className="fa fa-home" />
                                        {!this.props.getMoveProfile.redirect && !this.props.changePassword.inChangePassword ? 'Profile' : 'NewsFeed'}
                                        <span className="sr-only">(current)</span>
                                    </a>
                                </li>
                                <ul onClick={evt => this.changePassword(evt)} className="navbar-nav">
                                    <NavItem classIcon='fa fa-key' itemName='Change Password' />
                                </ul>
                                <NavItem classIcon='fa fa-envelope-o' classNote='badge badge-danger' numberNode='20' itemName='Message' />
                            </ul>
                            <form onSubmit={evt => this.handleOnSearch(evt)} className="form-inline my-2 my-lg-0">
                                <input onChange={evt => this.handleSearch(evt)} className="form-control input-search-posts mr-sm-2" type="text" placeholder="Search" aria-label="Search" />
                                <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                            </form>
                            <ul onClick={evt => this.check(evt)} className="navbar-nav">
                                <NavItem classIcon='fa fa-sign-out' classNote='badge badge-info' />
                            </ul>
                        </div>
                    </nav>
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        newsfeed: state.NewsFeed,
        getMoveProfile: state.MoveProfile,
        displayBtnNewsFeed: state.HandleInputSignin,
        changePassword: state.changePassword
    }
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        setRedirectProfile: () => {
            dispatch(actions.setRedirectProfile());
        },
        getInputSearch: (keyword) => {
            dispatch(actions.getInputSearch(keyword));
        },
        onSearch: (value) => {
            dispatch(actions.onSearch(value));
        },
        leaveChangePassword: () => {
            dispatch(leaveChangePassword());
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);