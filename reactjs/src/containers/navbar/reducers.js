import * as types from './../../constants/actions/navbar';
import {getNewsFeedSuccess} from './../../constants/actions/newsfeed';

const initialState = {
    redirect: false,
    keyword: '',
    postsSearch: [],
    postsSearchCopy: [],
};

var redirectProfile = (state = initialState, action) => {
    switch (action.type) {
        case types.setRedirectProfile:
            return {
                ...state,
                redirect: !state.redirect
            };
        case types.getInputSearch:
            state.postsSearch = state.postsSearchCopy.filter((postSearch) => {
                return postSearch.content.toLowerCase().indexOf(action.keyword) !== -1;
            });
            return {
                ...state,
                keyword: action.keyword
            };
        case getNewsFeedSuccess:
            state.postsSearchCopy = action.posts;
            return {
                ...state,
                postsSearch: action.posts,
                redirect: false
            };
        default:
            return state;
    }
};

export default redirectProfile;