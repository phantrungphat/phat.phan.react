import * as types from './../../constants/actions/navbar';

export const setRedirectProfile = () => {
    return {
        type: types.setRedirectProfile,
    }
};

export const getInputSearch = (keyword) => {
    return {
        type: types.getInputSearch,
        keyword
    }
};

export const onSearch = (value) => {
    return {
        type: types.onSearch,
        value
    }
};


