import * as types from './../../constants/actions/signin';

export const handleInput = (key, value) => {
    return {
        type: types.handleInput,
        key,
        value
    }
};

export const requestSignin = () => {
    return {
        type: types.requestSignin
    }
};

export const signinSuccess = () => {
    return {
        type: types.signinSuccess
    }
};