import * as configHost from './../../constants/host/config';
import * as configApi from './../../constants/api/config';

import {
    fork,
    select,
    put
} from 'redux-saga/effects';
import {
    push
} from 'connected-react-router';
import {
    signinSuccess
} from './actions';
import {
    responseWithError
} from './../popup/actions';
import axios from 'axios';

const CancelToken = axios.CancelToken;

let cancelApi;

function* signinApi() {
    try {
        const getStateSignin = yield select(state => state.HandleInputSignin);
        if (cancelApi !== undefined) {
            cancelApi();
        }
        const respone = yield axios.post(`${configHost.apiUrl}/${configApi.apiSignin}`,
            {
                emailOrPhone: getStateSignin.emailOrPhone,
                password: getStateSignin.password
            }, {
                cancelToken: new CancelToken(function executor(c) {
                    cancelApi = c;
                })
            });
        const responeJson = respone.data;
        if (!responeJson.error) {
            localStorage.setItem('token', responeJson.data.apiToken);
            yield put(push('/'));
            yield put(signinSuccess());
        }
    } catch (error) {
        if (axios.isCancel(error)) {
            console.log('post Request canceled');
        }
        let response = error.response.data;
        if (response.error) {
            localStorage.clear();
            yield put(responseWithError(response.errors.errorMessage));
        }
    }
}

export default function* signinPage() {
    yield fork(signinApi);
}