import React from 'react';
import signinAvatar from './../../image/avatar.jpeg';
import { Input } from './../../components/input';
import { Validation } from './../../components/validateInput';
import Link from './../../components/link';
import { connect } from 'react-redux';
import * as actions from './actions';
import { requestGetProfile } from './../profilePage/actions';
import { checkAuth } from './../../components/checkAuth';
import Popup from './../../containers/popup';
import './style.css';

class signinPage extends React.Component {

  componentWillMount() {
    checkAuth();
  }

  handleInput = (key, event) => {
    this.props.onHandleInput(key, event.target.value);
  }

  handleSubmit = (evt) => {
    evt.preventDefault();
    if (this.props.handleInput.isValidEmail && this.props.handleInput.isValidPassword) {
      localStorage.setItem('emailOrPhone', this.props.handleInput.emailOrPhone);
      this.props.requestSignin();
    }
  }

  render() {
    const { emailError, passwordError } = this.props.handleInput;
    return (

      <div className="form-edit">
        <div className="h-100">
          <div className="d-flex justify-content-center h-100">
            <div className="user_card">
              <Popup />
              <div className="d-flex justify-content-center">
                <div className="brand_logo_container">
                  <img src={signinAvatar} className="brand_logo" alt="Logo" />
                </div>
              </div>
              <div className="d-flex justify-content-center form_container">
                <form onSubmit={this.handleSubmit}>

                  <Input type='text' placeholder='email or phone' onInputChange={value => this.handleInput('emailOrPhone', value)} classIcon='fa fa-user'></Input>

                  <Validation message={emailError}></Validation>

                  <Input type='password' placeholder='password' onInputChange={value => this.handleInput('password', value)} classIcon='fa fa-unlock-alt'></Input>

                  <Validation message={passwordError}></Validation>

                  <div className="d-flex justify-content-center mt-3 login_container">
                    <button type="submit" name="button" className="btn login_btn">Sign In</button>
                  </div>
                </form>
              </div>
              <div className="mt-4">
                <Link href='/signup' classSymmetric='ml-2' content='Sign Up'>Don't have an account?</Link>
                <Link href='/requestforgot' classSymmetric='' content='Forgot your password?' />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    profile: state.Profile,
    handleInput: state.HandleInputSignin
  }
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    requestGetProfile: () => {
      dispatch(requestGetProfile());
    },
    onHandleInput: (key, value) => {
      dispatch(actions.handleInput(key, value));
    },
    requestSignin: () => {
      dispatch(actions.requestSignin());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(signinPage);
