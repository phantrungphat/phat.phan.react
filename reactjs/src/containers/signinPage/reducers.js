import * as types from './../../constants/actions/signin';
import {getProfileSuccess} from './../../constants/actions/profile';

const initialState = {
    emailOrPhone: '',
    password: '',
    emailError: '',
    passwordError: '',
    isValidEmail: false,
    isValidPassword: false,
    isSignin: false
};

var isValidEmail = (emailOrPhone) => {
    try {
        if (emailOrPhone === '') return false;
        if (!/^[a-zA-Z0-9._%+-]{2,}@[a-zA-Z0-9.-]{2,}(\.[a-z]{2,})$/.test(emailOrPhone)) {
            return false;
        } return true;
    } catch (error) {
        alert('Something went wrong!!!');
    }
}

var checkLengthPwd = (password) => {
    try {
        if (password.length < 6) {
            return 'The password too Short';
        }
        else if (password.length > 16) {
            return 'The password too Long';
        }
        else return '';
    } catch (error) {
        alert(error);
    }
}

var signinReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.handleInput:
            if (action.key === 'emailOrPhone') {
                if (isValidEmail(action.value)) {
                    state.emailError = '';
                    state.isValidEmail = true;
                }
                else {
                    state.emailError = 'The email is invalid';
                    state.isValidEmail = false;
                }
            } else {
                let checkPassword = checkLengthPwd(action.value);
                if (checkPassword !== '') {
                    state.isValidPassword = false;
                }
                else {
                    state.isValidPassword = true;
                }
                state.passwordError = checkPassword;
            }
            return {
                ...state,
                [action.key]: action.value
            };
        case types.signinSuccess:
            return {
                ...state,
                isSignin: true
            };
        case getProfileSuccess:
            return {
                ...state,
            };
        default:
            return state;
    }
};

export default signinReducer;