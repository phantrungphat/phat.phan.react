import * as types from './../../constants/actions/changePassword';

const initialState = {
    password: '',
    newPassword: '',
    reNewPassword: '',
    passwordError: '',
    newPasswordError: '',
    reNewPasswordError: '',
    isValidPassword: false,
    isValidNewPassword: false,
    isValidReNewPassword: false,
    inChangePassword: false
};

var checkLengthPwd = (password) => {
    try {
        if (password.length < 6) {
            return 'The password too Short';
        }
        else if (password.length > 16) {
            return 'The password too Long';
        }
        else return '';
    } catch (error) {
        alert(error);
    }
}

var isValidRePassword = (password, rePassword) => {
    try {
        if (password === '') return false;
        if (password !== rePassword) {
            return false;
        } return true;
    } catch (error) {
        alert(error);
    }
}

var changePassword = (state = initialState, action) => {
    switch (action.type) {
        case types.handleInput:
            if (action.key === 'password') {
                let checkPassword = checkLengthPwd(action.value);
                if (checkPassword !== '') {
                    state.isValidPassword = false;
                }
                else {
                    state.isValidPassword = true;
                }
                state.passwordError = checkPassword;
            }
            else if (action.key === 'newPassword') {
                let checkPassword = checkLengthPwd(action.value);
                if (checkPassword !== '') {
                    state.isValidNewPassword = false;
                }
                else {
                    state.isValidNewPassword = true;
                }
                if (state.password === action.value) {
                    checkPassword = 'Old Password and New Password are match';
                    state.isValidNewPassword = false;
                }
                state.newPasswordError = checkPassword;
            }
            else {
                if (isValidRePassword(state.newPassword, action.value))
                {
                    state.reNewPasswordError = '';
                    state.isValidReNewPassword = true;
                }
                else {
                    state.reNewPasswordError = 'NewPassword and Comfirm Password are not match';
                    state.isValidReNewPassword = false;
                }
            }
            return {
                ...state,
                [action.key]: action.value,

            };
        case types.changePasswordSuccess:
            return {
                ...state,
            };
        case types.inChangePassword:
            return {
                ...state,
                inChangePassword: true
            };
        case types.leaveChangePassword:
            return {
                ...state,
                inChangePassword: false
            };
        default:
            return state;
    }
};

export default changePassword;