import * as types from './../../constants/actions/changePassword';

export const handleInput = (key, value) => {
    return {
        type: types.handleInput,
        key,
        value
    }
};

export const requestChangePassword = () => {
    return {
        type: types.requestChangePassword,
    }
};

export const changePasswordSuccess = () => {
    return {
        type: types.changePasswordSuccess,
    }
};

export const inChangePassword = () => {
    return {
        type: types.inChangePassword,
    }
};

export const leaveChangePassword = () => {
    return {
        type: types.leaveChangePassword,
    }
};