import React from 'react';
import { InputBorder } from './../../components/inputBorder';
import * as actions from './actions';
import { connect } from 'react-redux';
import { Validation } from './../../components/validateInput';
import Loading from './../globalLoading';
import Popup from './../../containers/popup';
import './style.css';

class ChangePassword extends React.Component {

    handleInput = (key, event) => {
        this.props.handleInput(key, event.target.value);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if (this.props.changePassword.isValidPassword && this.props.changePassword.isValidNewPassword && this.props.changePassword.isValidReNewPassword) 
            this.props.requestChangePassword();
    }

    componentWillMount() {
        this.props.inChangePassword();
    }

    render() {
        const { passwordError, newPasswordError, reNewPasswordError } = this.props.changePassword;
        return (
            <div>
                <Loading />
                <Popup />
                <div className="container-fluid">
                    <div className="row d-flex justify-content-center edit-height">
                        <div className="col-md-6">
                            <div className="card card-change-password flex-row">
                                <div className="card-body">
                                    <h5 className="card-title text-center mb-4"><strong>Change Your Password</strong></h5>
                                    <form onSubmit={this.handleSubmit} className="form-signin">
                                        <InputBorder
                                            onInputChange={value => this.handleInput('password', value)}
                                            type='password' idLabel='oldPassword'
                                            plholder='Enter your Old Password'
                                        />
                                        <Validation message={passwordError}></Validation>
                                        <InputBorder
                                            onInputChange={value => this.handleInput('newPassword', value)}
                                            type='password' idLabel='newPassword'
                                            plholder='Enter your New Password'
                                        />
                                        <Validation message={newPasswordError}></Validation>
                                        <InputBorder
                                            onInputChange={value => this.handleInput('reNewPassword', value)}
                                            type='password' idLabel='reNewPassword'
                                            plholder='Please Enter your New Password again'
                                        />
                                        <Validation message={reNewPasswordError}></Validation>
                                        <button className="btn btn-lg btn-success btn-block text-uppercase btn-editprofile" type="submit">Save</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        changePassword: state.changePassword
    }
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        handleInput: (key, value) => {
            dispatch(actions.handleInput(key, value));
        },
        requestChangePassword: () => {
            dispatch(actions.requestChangePassword());
        },
        inChangePassword: () => {
            dispatch(actions.inChangePassword());
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);