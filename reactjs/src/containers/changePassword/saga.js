import * as configHost from './../../constants/host/config';
import * as configApi from './../../constants/api/config';
import { select, fork, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import {responseWithError} from './../popup/actions';
import {showLoading, hideLoading} from './../globalLoading/actions';

function* changePasswordApi() {
    try {
        const getStateChangePassword = yield select(state => state.changePassword);

        let params = {
            oldpassword: getStateChangePassword.password,
            newpassword: getStateChangePassword.newPassword,
            renewpassword: getStateChangePassword.reNewPassword
        };

        const token = yield localStorage.getItem('token');
        const data = yield fetch(`${configHost.apiUrl}/${configApi.apiChangePassword}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                'apiToken': token
            },
            body: JSON.stringify(params)
        });
        const respone = yield data.json();
        yield put(hideLoading());
        if (!respone.error) {
            alert(respone.data);
            localStorage.clear();
            yield put(push('/signin'));
        }
        else {
            console.log(respone.errors.errorMessage);
            yield put(responseWithError(respone.errors.errorMessage));
        }
    }
    catch (error) {
        yield put(hideLoading());
        yield put(responseWithError(error));
    }
}
export default function* changePassword() {
    yield put(showLoading());
    yield fork(changePasswordApi);
}
