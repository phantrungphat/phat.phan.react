import * as configHost from './../../constants/host/config';
import * as configApi from './../../constants/api/config';
import { fork, put } from 'redux-saga/effects';
import * as actions from './actions';
import {responseWithError} from './../popup/actions';
import {showLoading, hideLoading} from './../globalLoading/actions';

function* getFriendListApi() {
    try {
        const token = yield localStorage.getItem('token');
        const data = yield fetch(`${configHost.apiUrl}/${configApi.apiGetListFriend}`, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json',
                'apiToken': token
            }
        });
        const respone = yield data.json();
        yield put(hideLoading());
        if (!respone.error) {
            yield put(actions.getFriendListSuccess(respone.data));
        }
        else {
            localStorage.clear();
            yield put(responseWithError(respone.errors));
        }
    }
    catch (error) {
        yield put(hideLoading());
        yield put(responseWithError(error));
    }
}

export default function* formPost() {
    yield put(showLoading());
    yield fork(getFriendListApi);
}
