import * as types from './../../constants/actions/friendList';

export const getFriendList = () => {
    return {
        type: types.getFriendList
    }
};

export const getFriendListSuccess = (data) => {
    return {
        type: types.getFriendListSuccess,
        data
    }
};
