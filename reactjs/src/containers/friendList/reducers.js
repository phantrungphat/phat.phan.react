import * as types from './../../constants/actions/friendList';

const initialState = {
    listFriend: [],
    emailOrPhone: ''
};

var friendList = (state = initialState, action) => {
    switch (action.type) {
        case types.getFriendListSuccess:
            state.emailOrPhone = localStorage.getItem('emailOrPhone');;
            return {
                ...state,
                listFriend: action.data
            };
        default:
            return state;
    }
};

export default friendList;