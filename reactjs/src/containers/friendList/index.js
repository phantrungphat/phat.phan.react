import React from 'react';
import './style.css';
import { connect } from 'react-redux';
import * as actions from './actions';
import Loading from './../globalLoading';
import Popup from './../../containers/popup';

class FriendList extends React.Component {

    componentWillMount() {
        this.props.getFriendList();
    }

    render() {
        const { listFriend, emailOrPhone } = this.props.friendList;
        return (
            <div>
                <Loading />
                <Popup />
                <div className="card" id="accordion">
                    <h3><b>Friend List</b></h3>
                    {
                        listFriend.map((friend, index) => {
                            return friend.email_phone !== emailOrPhone &&
                                <div key={index} className="card card-list-friend mb-2">
                                    <button className="btn" data-toggle="collapse" data-target={"#idFriend" + index}>
                                        <div className="card-title text-white">{friend.email_phone}</div>
                                    </button>
                                    <div id={"idFriend" + index} className="collapse" data-parent="#accordion">
                                        <div className="card-body">
                                            <div className="card-text"><i className="fa fa-venus-double"></i> {friend.gender}</div>
                                            <div className="card-text"><i className="fa fa-birthday-cake profile-icon"></i> {friend.dob}</div>
                                            <div className="card-text"><i className="fa fa-align-right profile-icon"></i> {friend.about}</div>
                                        </div>
                                    </div>
                                </div>
                        })
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        friendList: state.friendList,
        infoSignin: state.HandleInputSignin
    }
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        getFriendList: () => {
            dispatch(actions.getFriendList());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(FriendList);