import * as configHost from './../../constants/host/config';
import * as configApi from './../../constants/api/config';
import { select, fork, put } from 'redux-saga/effects';
import { editProfileSuccess } from './actions';
import { requestGetProfile } from './../profilePage/actions';
import { push } from 'connected-react-router';
import {showLoading,hideLoading} from './../globalLoading/actions';
import {responseWithError} from './../popup/actions';

function* putEditProfileApi() {
    try {
        const getStateProfile = yield select(state => state.EditProfile);

        let params = {
            dob: getStateProfile.dob,
            about: getStateProfile.about,
            gender: getStateProfile.gender
        };

        const token = yield localStorage.getItem('token');
        const data = yield fetch(`${configHost.apiUrl}/${configApi.apiEditProfile}`, {
            method: "PUT",
            headers: {
                'Content-Type': 'application/json',
                'apiToken': token
            },
            body: JSON.stringify(params)
        });
        const respone = yield data.json();
        yield put(hideLoading());
        if (!respone.error) {
            yield put(editProfileSuccess());
            yield put(requestGetProfile());
        }
        else {
            localStorage.clear();
            yield put(push('/signin'));
            yield put(responseWithError(respone.errors));
        }
    }
    catch (error) {
        yield put(hideLoading());
        yield put(responseWithError(error));
    }
}
export default function* editProfilePage() {
    yield put(showLoading());
    yield fork(putEditProfileApi);
}
