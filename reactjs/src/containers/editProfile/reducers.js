import * as types from './../../constants/actions/editProfile';
import {checkLength} from './../../components/checkLength';

const initialState = {
    dob: '',
    about: '',
    gender: '',
    aboutError: '',
    isValid: false,
    isChange: false
};

var editProfile = (state = initialState, action) => {
    switch (action.type) {
        case types.handleInputEdit:
            if (action.key === 'about') {
                let checkAbout = checkLength(action.value);
                if (checkAbout !== '') {
                    state.isValidAbout = false;
                }
                else {
                    state.isValidAbout = true;
                }
                state.aboutError = checkAbout;
            }
            return {
                ...state,
                [action.key]: action.value,
                isChange: true
            };
        case types.getProfileNow:
            if (action.about.length <= 100) state.isValidAbout = true;
            return {
                ...state,
                gender: action.gender,
                dob: action.dob,
                about: action.about,
                isChange: false
            };
        case types.editProfileSuccess:
            return {
                ...state
            };
        default:
            return state;
    }
};

export default editProfile;