import * as types from './../../constants/actions/editProfile';

export const requestEditProfile = () => {
    return {
        type: types.requestEditProfile
    }
};

export const handleInputEdit = (key, value) => {
    return {
        type: types.handleInputEdit,
        key,
        value
    }
};

export const editProfileSuccess = () => {
    return {
        type: types.editProfileSuccess
    }
};

export const getProfileNow = (dob, about, gender) => {
    return {
        type: types.getProfileNow,
        dob,
        about,
        gender
    }
};