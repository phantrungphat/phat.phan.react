import React from 'react';
import { InputBorder } from './../../components/inputBorder';
import './style.css';
import { connect } from 'react-redux';
import * as actions from './actions';
import {Validation} from './../../components/validateInput';
import {checkAuth} from './../../components/checkAuth';
import Loading from './../globalLoading';
import Popup from './../../containers/popup';

class EditProfile extends React.Component {

    handleInput = (key, event) => {
        this.props.handleInputEdit(key, event.target.value);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log()
        if (!this.props.editProfile.isChange) alert('Nothing to Edit your Profile');
        else if (this.props.editProfile.isValidAbout) this.props.requestEditProfile();
    }

    componentWillMount() {
        checkAuth();
        this.props.getProfileNow(this.props.profile.dob, this.props.profile.about, this.props.profile.gender);
    }

    render() {
        const { gender, dob, about, aboutError } = this.props.editProfile;
        return (
            <div>
                <Loading />
                <Popup />
                <div className="card card-signin flex-row">
                    <div className="card-body">
                        <h5 className="card-title text-center"><strong>Edit Profile</strong></h5>
                        <form onSubmit={this.handleSubmit} className="form-signin">
                            <InputBorder
                                onInputChange={value => this.handleInput('dob', value)}
                                type='date' idLabel='inputDob'
                                plholder='Edit your Day of Birth'
                                defaultValue={dob}
                            />
                            <InputBorder
                                onInputChange={value => this.handleInput('about', value)}
                                type='text' idLabel='inputAbout'
                                plholder='Edit About you'
                                defaultValue={about}
                            />
                            <Validation message={aboutError}></Validation>
                            <div className="gender-edit d-flex justify-content-center">
                                <select value={gender} onChange={evt => this.handleInput('gender', evt)} className="form-control selcls">
                                    <option value='male'>Male</option>
                                    <option value='female'>Female</option>
                                </select>
                            </div>
                            <button className="btn btn-lg btn-success btn-block text-uppercase btn-editprofile" type="submit">Save</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        editProfile: state.EditProfile,
        profile: state.Profile
    }
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        getProfileNow: (dob, about, gender) => {
            dispatch(actions.getProfileNow(dob, about, gender));
        },
        handleInputEdit: (key, value) => {
            dispatch(actions.handleInputEdit(key, value));
        },
        requestEditProfile: () => {
            dispatch(actions.requestEditProfile());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);