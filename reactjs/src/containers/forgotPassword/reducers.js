import * as types from './../../constants/actions/forgotPassword';

const initialState = {
    newPassword: '',
    reNewPassword: '',
    newPasswordError: '',
    reNewPasswordError: '',
    isValidNewPassword: false,
    isValidReNewPassword: false
};

var checkLengthPwd = (password) => {
    try {
        if (password.length < 6) {
            return 'The password too Short';
        }
        else if (password.length > 16) {
            return 'The password too Long';
        }
        else return '';
    } catch (error) {
        alert(error);
    }
}

var isValidRePassword = (password, rePassword) => {
    try {
        if (password === '') return false;
        if (password !== rePassword) {
            return false;
        } return true;
    } catch (error) {
        alert(error);
    }
}

var forgotPassword = (state = initialState, action) => {
    switch (action.type) {
        case types.handleInput:
            if (action.key === 'newPassword') {
                let checkPassword = checkLengthPwd(action.value);
                if (checkPassword !== '') {
                    state.isValidNewPassword = false;
                }
                else {
                    state.isValidNewPassword = true;
                }
                state.newPasswordError = checkPassword;
            }
            else {
                if (isValidRePassword(state.newPassword, action.value))
                {
                    state.reNewPasswordError = '';
                    state.isValidReNewPassword = true;
                }
                else {
                    state.reNewPasswordError = 'NewPassword and Comfirm Password are not match';
                    state.isValidReNewPassword = false;
                }
            }
            return {
                ...state,
                [action.key]: action.value,
            };
        case types.forgotPasswordSuccess:
            return {
                ...state,
            };
        default:
            return state;
    }
};

export default forgotPassword;