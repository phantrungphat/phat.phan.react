import * as configHost from './../../constants/host/config';
import * as configApi from './../../constants/api/config';
import { select, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import {responseWithError} from './../popup/actions';
import {showLoading, hideLoading} from './../globalLoading/actions';

export default function* forgotPasswordApi(action) {
    try {
        yield put(showLoading());
        const getStateForgot = yield select(state => state.forgotPassword);
        let params = {
            password: getStateForgot.newPassword,
            repassword: getStateForgot.reNewPassword,
            token: action.param.params.token
        };
        const data = yield fetch(`${configHost.apiUrl}/${configApi.apiForgotPassword}`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(params)
        });
        const respone = yield data.json();
        yield put(hideLoading());
        if (!respone.error) {
            yield put(responseWithError(respone.data));
            localStorage.clear();
            yield put(push('/signin'));
        }
        else {
            yield put(hideLoading());
            yield put(responseWithError(respone.errors.errorMessage));
        }
    }
    catch (error) {
        yield put(responseWithError(error));
    }
}