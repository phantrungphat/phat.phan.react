import * as types from './../../constants/actions/forgotPassword';

export const handleInput = (key, value) => {
    return {
        type: types.handleInput,
        key,
        value
    }
};

export const requestChangeForgot = (param) => {
    return {
        type: types.requestChangeForgot,
        param
    }
};

export const forgotPasswordSuccess = () => {
    return {
        type: types.forgotPasswordSuccess,
    }
};
