import React from 'react';
import { InputBorder } from './../../components/inputBorder';
import * as actions from './actions';
import { connect } from 'react-redux';
import { Validation } from './../../components/validateInput';
import './style.css';
import Loading from './../globalLoading';
import Popup from './../../containers/popup';
import {Link} from 'react-router-dom';

class ForgotPassword extends React.Component {

    handleInput = (key, event) => {
        this.props.handleInput(key, event.target.value);
    }

    handleSubmit = (event) => {
        event.preventDefault();
        if (this.props.forgotPassword.isValidNewPassword && this.props.forgotPassword.isValidReNewPassword) 
        {
            this.props.requestChangeForgot(this.props.match);
        }
    }
    render() {
        const { newPasswordError, reNewPasswordError } = this.props.forgotPassword;
        return (
            <div>
                <Loading />
                <Popup />
                <div className="container-fluid">
                    <div className="row d-flex justify-content-center edit-height">
                        <div className="col-md-6">
                            
                            <div className="card card-change-password flex-row">
                                <div className="card-body">
                                <Link className="button-back-signin" to={'/'} >Home</Link>
                                    <h5 className="card-title text-center mb-4"><strong>Change Your Password</strong></h5>
                                    <form onSubmit={this.handleSubmit} className="form-signin">
                                        <InputBorder
                                            onInputChange={value => this.handleInput('newPassword', value)}
                                            type='password' idLabel='newPassword'
                                            plholder='Enter your New Password'
                                        />
                                        <Validation message={newPasswordError}></Validation>
                                        <InputBorder
                                            onInputChange={value => this.handleInput('reNewPassword', value)}
                                            type='password' idLabel='reNewPassword'
                                            plholder='Please Enter your New Password again'
                                        />
                                        <Validation message={reNewPasswordError}></Validation>
                                        <button className="btn btn-lg btn-success btn-block text-uppercase btn-editprofile" type="submit">Save</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        forgotPassword: state.forgotPassword
    }
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        handleInput: (key, value) => {
            dispatch(actions.handleInput(key, value));
        },
        requestChangeForgot: (param) => {
            dispatch(actions.requestChangeForgot(param));
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);