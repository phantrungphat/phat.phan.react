import * as types from './../../constants/actions/popup';

export const responseWithError = (errorMessage) => {
    return {
        type: types.responseWithError,
        errorMessage
    }
};

export const closePopup = () => {
    return {
        type: types.closePopup,
    }
};

