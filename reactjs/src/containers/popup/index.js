import React from 'react';
import Modal from 'react-awesome-modal';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from './actions';
import './style.css';

class Popup extends React.Component {

  handleClosePopup = (evt) => {
    evt.preventDefault();
    this.props.closePopup();
  }

  render() {
    const { error, errorMessage } = this.props.popup;
    return (

      <Modal visible={error} width="400" height="150" effect="fadeInUp">
        <div>
          <h1 className="popup-header">Pop up Error</h1>
          <p className="popup-content">{errorMessage}</p>
          <Link to="/" onClick={evt => this.handleClosePopup(evt)} className="popup-button-close">Close</Link>
        </div>
      </Modal>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    popup: state.popup
  }
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    closePopup: () => {
      dispatch(actions.closePopup());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Popup);
