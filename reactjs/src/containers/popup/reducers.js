import * as types from './../../constants/actions/popup';

const initialState = {
    error: false,
    errorMessage: ''
};

var popup = (state = initialState, action) => {
    switch (action.type) {
        case types.responseWithError:
            return {
                ...state,
                error: true,
                errorMessage: action.errorMessage
            };
        case types.closePopup:
            return {
                ...state,
                error: false,
                errorMessage: ''
            };
        default:
            return state;
    }
};

export default popup;