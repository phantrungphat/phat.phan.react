import * as types from './../../constants/actions/globalLoading';

const initialState = {
    showLoading: false
};


var Loading = (state = initialState, action) => {
    switch (action.type) {
        case types.showLoading:
            return {
                ...state,
                showLoading: true
            };
        case types.hideLoading:
            return {
                ...state,
                showLoading: false
            }
        default:
            return state;
    }
};

export default Loading;