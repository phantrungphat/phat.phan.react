import React from 'react';
import { connect } from 'react-redux';
import './style.css';
import LoadingIcon from './../../image/loading.gif';

class GlobalLoading extends React.Component {
    render() {
        return (
            <div>
                {
                    this.props.Loading.showLoading &&
                    <div className="loading">
                        <img src={LoadingIcon} alt="loading" className="loading-img" />
                    </div>
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        Loading: state.Loading
    }
};

export default connect(mapStateToProps, null)(GlobalLoading);