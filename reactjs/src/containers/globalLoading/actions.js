import * as types from './../../constants/actions/globalLoading';

export const showLoading = () => {
    return {
        type: types.showLoading,
    }
};

export const hideLoading = () => {
    return {
        type: types.hideLoading,
    }
};