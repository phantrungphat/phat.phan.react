import * as types from './../../constants/actions/profile';

const initialState = {
    emailOrPhone: '',
    gender: '',
    dob: '',
    about: '',
    redirect: false
};

var profile = (state = initialState, action) => {
    switch (action.type) {
        case types.getProfileSuccess:
            return {
                ...state,
                emailOrPhone: action.data.email_phone,
                gender: action.data.gender,
                dob: action.data.dob,
                about: action.data.about,
            };
        case types.toEditProfile:
            return {
                ...state,
                redirect: !state.redirect
            };
        case types.setDefaultRedirect:
            return {
                ...state,
                redirect: false
            };
            default:
                return state;
    }
};

export default profile;