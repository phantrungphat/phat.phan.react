import * as types from './../../constants/actions/profile';

export const requestGetProfile = () => {
    return {
        type: types.requestGetProfile
    }
};

export const getProfileSuccess = (data) => {
    return {
        type: types.getProfileSuccess,
        data
    }
};

export const toEditProfile = () => {
    return {
        type: types.toEditProfile,
    }
};

export const setDefaultRedirect = () => {
    return {
        type: types.setDefaultRedirect,
    }
};