import React from 'react';
import './style.css';
import { connect } from 'react-redux';
import * as actions from './actions';
import { checkAuth } from './../../components/checkAuth';
import avatar from './../../image/avatarMan.jpg';
import Loading from './../globalLoading';
import Popup from './../../containers/popup';

class Profile extends React.Component {

  componentWillMount() {
    checkAuth();
    this.props.setDefaultRedirect();
    this.props.requestGetProfile();
  }

  handleEdit = (evt) => {
    evt.preventDefault();
    this.props.toEditProfile();
  }

  render() {
    const { emailOrPhone, gender, dob, about } = this.props.profile;
    return (
      <div className="wrapper">
        <Loading />
        <Popup />
        <div className="card" style={{ width: '100%' }}>
          <img className="card-img-top" src={avatar} alt="avatar" style={{ width: '100%' }} />
          <div className="card-body">
            <div className="col-md-3"></div>
            <h6 className="card-title"><i className="fa fa-envelope profile-icon"></i> {emailOrPhone}</h6>
            <div className="card-gender"><i className="fa fa-venus-double"></i> {gender}</div>
            <div className="card-birth"><i className="fa fa-birthday-cake profile-icon"></i> {dob}</div>
            <div className="card-text"><i className="fa fa-align-right profile-icon"></i> {about}</div>
          </div>
          <button onClick={evt => this.handleEdit(evt)} className="btn btn-default btn-profile btn-block text-uppercase" type="button">Edit</button>
          <div className="card-footer text-muted">Profile of {emailOrPhone}</div>
        </div>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    profile: state.Profile
  }
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    requestGetProfile: () => {
      dispatch(actions.requestGetProfile());
    },
    toEditProfile: () => {
      dispatch(actions.toEditProfile());
    },
    setDefaultRedirect: () => {
      dispatch(actions.setDefaultRedirect());
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
