import * as configHost from './../../constants/host/config';
import * as configApi from './../../constants/api/config';
import { fork, put } from 'redux-saga/effects';
import { push } from 'connected-react-router';
import { getProfileSuccess } from './actions';
import axios from 'axios';
import { showLoading, hideLoading } from './../globalLoading/actions';
import { responseWithError } from './../popup/actions';

function* getProfileApi() {
  try {
    const token = localStorage.getItem('token');
    const respone = yield axios(`${configHost.apiUrl}/${configApi.apiProfile}`, {
      method: "GET",
      headers: {
        'Content-Type': 'application/json',
        'apiToken': token
      },
      timeout: 1000
    });
    const responeJson = yield respone.data;
    yield put(hideLoading());
    if (!responeJson.error) {
      yield put(getProfileSuccess(responeJson.data));
    }
    else {
      localStorage.clear();
      yield put(push('/signin'));
      yield put(responseWithError(responeJson.errors));
    }
  }
  catch (error) {
    yield put(hideLoading());
    yield put(responseWithError(error));
  }
}
export default function* profilePage() {
  yield put(showLoading());
  yield fork(getProfileApi);
}
