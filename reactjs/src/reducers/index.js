import { combineReducers } from 'redux';
import HandleInputSignin from '../containers/signinPage/reducers';
import HandleInputSignup from '../containers/signupPage/reducers';
import NewsFeed from './../containers/newsfeedPage/reducers';
import MoveProfile from './../containers/navbar/reducers';
import Profile from './../containers/profilePage/reducers';
import EditProfile from './../containers/editProfile/reducers';
import { connectRouter } from 'connected-react-router';
import postNewsFeed from './../containers/formPost/reducers';
import friendList from './../containers/friendList/reducers';
import changePassword from './../containers/changePassword/reducers';
import requestForgot from './../containers/requestForgot/reducers';
import forgotPassword from './../containers/forgotPassword/reducers';
import Loading from './../containers/globalLoading/reducers';
import popup from './../containers/popup/reducers';

export default (history) => combineReducers ({
    router: connectRouter(history),
    HandleInputSignin,
    HandleInputSignup,
    NewsFeed,
    MoveProfile,
    Profile,
    EditProfile,
    postNewsFeed,
    friendList,
    changePassword,
    requestForgot,
    forgotPassword,
    Loading,
    popup
}); 